package pl.pa.auto.service.airconditioning;

public enum AirConditioningDefect {
    FUNGUS(200, 2),
    UNFILLING(200, 2),
    LEAKY(200, 2),
    BROKEN(600, 4);

    private double repairPrice;
    private int repairTimeDays;

    AirConditioningDefect(double repairPrice, int repairTimeDays) {
        this.repairPrice = repairPrice;
        this.repairTimeDays = repairTimeDays;
    }

    public double getRepairPrice() {
        return repairPrice;
    }

    public int getRepairTimeDays() {
        return repairTimeDays;
    }
}
