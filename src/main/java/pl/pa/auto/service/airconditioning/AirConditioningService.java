package pl.pa.auto.service.airconditioning;

import pl.pa.auto.service.common.Car;

public class AirConditioningService {

    public double calculateRepairPrice(Car car) {
        double repairPrice = 0;

        for (int i = 0; i < car.defectListAirConditioning().size(); i++) {
            repairPrice += car.defectListAirConditioning().get(i).getRepairPrice();
        }
        return repairPrice;
    }

    public int calculateRepairTimeInDays(Car car) {
        int repairTimeInDays = 0;

        for (int i = 0; i < car.defectListAirConditioning().size(); i++) {
            repairTimeInDays += car.defectListAirConditioning().get(i).getRepairTimeDays();
        }
        return repairTimeInDays;
    }
}
