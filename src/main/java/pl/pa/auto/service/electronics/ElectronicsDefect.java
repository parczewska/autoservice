package pl.pa.auto.service.electronics;

public enum ElectronicsDefect {

    SYSTEMS_REPAIR(300, 2),
    CHANGE_OF_SENSORS(700, 3);

    private double repairPrice;
    private int repairTimeDays;

    ElectronicsDefect(double repairPrice, int repairTimeDays) {
        this.repairPrice = repairPrice;
        this.repairTimeDays = repairTimeDays;
    }

    public double getRepairPrice() {
        return repairPrice;
    }

    public int getRepairTimeDays() {
        return repairTimeDays;
    }
}
