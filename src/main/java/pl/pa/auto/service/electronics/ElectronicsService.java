package pl.pa.auto.service.electronics;

import pl.pa.auto.service.common.Car;

public class ElectronicsService {

    public double calculateRepairPrice(Car car) {
        double repairPrice = 0;

        for (int i = 0; i < car.defectListElectronics().size(); i++) {
            repairPrice += car.defectListElectronics().get(i).getRepairPrice();
        }
        return repairPrice;
    }

    public int calculateRepairTimeInDays(Car car) {
        int repairTimeInDays = 0;

        for (int i = 0; i < car.defectListElectronics().size(); i++) {
            repairTimeInDays += car.defectListElectronics().get(i).getRepairTimeDays();
        }
        return repairTimeInDays;
    }
}
