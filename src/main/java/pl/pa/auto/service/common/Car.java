package pl.pa.auto.service.common;

import pl.pa.auto.service.airconditioning.AirConditioningDefect;
import pl.pa.auto.service.electronics.ElectronicsDefect;
import pl.pa.auto.service.mechanics.MechanicalDefect;
import pl.pa.auto.service.tinsmithing.TinsmithingDefect;
import pl.pa.auto.service.vulcanization.TireDefect;

import java.util.List;

public class Car {

    private String carModel;
    private String registrationNumber;
    private Owner owner;
    private Status status;
    private List<TireDefect> tireDefectList;
    private List<MechanicalDefect> mechanicalDefectList;
    private List<AirConditioningDefect> airConditioningDefectList;
    private List<TinsmithingDefect> tinsmithingDefectList;
    private List<ElectronicsDefect> electronicsDefectList;

    public Car(String carModel, String registrationNumber, Owner owner, Status status, List<TireDefect> tireDefectList,
               List<MechanicalDefect> mechanicalDefectList, List<AirConditioningDefect> airConditioningDefectList,
               List<TinsmithingDefect> tinsmithingDefectList, List<ElectronicsDefect> electronicsDefectList) {

        this.carModel = carModel;
        this.registrationNumber = registrationNumber;
        this.owner = owner;
        this.status = status;
        this.tireDefectList = tireDefectList;
        this.mechanicalDefectList = mechanicalDefectList;
        this.airConditioningDefectList = airConditioningDefectList;
        this.tinsmithingDefectList = tinsmithingDefectList;
        this.electronicsDefectList = electronicsDefectList;
    }

    public void addTireDefect(TireDefect tireDefect) {
        tireDefectList.add(tireDefect);
    }

    public void removeTireDefect(TireDefect tireDefect) {
        tireDefectList.remove(tireDefect);
    }

    public void addMechanicalDefect(MechanicalDefect mechanicalDefect) {
        mechanicalDefectList.add(mechanicalDefect);
    }

    public void removeMechanicalDefect(MechanicalDefect mechanicalDefect) {
        mechanicalDefectList.remove(mechanicalDefect);
    }

    public void addAirConditioningDefect(AirConditioningDefect airConditioningDefect) {
        airConditioningDefectList.add(airConditioningDefect);
    }

    public void removeAirConditioningDefect(AirConditioningDefect airConditioningDefect) {
        airConditioningDefectList.remove(airConditioningDefect);
    }

    public void addTinsmithingDefect(TinsmithingDefect tinsmithingDefect) {
        tinsmithingDefectList.add(tinsmithingDefect);
    }

    public void removeTinsmithingDefect(TinsmithingDefect tinsmithingDefect) {
        tinsmithingDefectList.add(tinsmithingDefect);
    }

    public void addElectronicsDefect(ElectronicsDefect electronicsDefect) {
        electronicsDefectList.add(electronicsDefect);
    }

    public void removeElectronicsDefect(ElectronicsDefect electronicsDefect) {
        electronicsDefectList.remove(electronicsDefect);
    }

    public List<TireDefect> defectListTire() {
        return tireDefectList;
    }

    public List<MechanicalDefect> defectListMechanical() {
        return mechanicalDefectList;
    }

    public List<AirConditioningDefect> defectListAirConditioning() {
        return airConditioningDefectList;
    }

    public List<TinsmithingDefect> defectListTinsmithing() {
        return tinsmithingDefectList;
    }

    public List<ElectronicsDefect> defectListElectronics() {
        return electronicsDefectList;
    }
}
