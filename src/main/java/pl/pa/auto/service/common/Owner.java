package pl.pa.auto.service.common;

public class Owner {

    private String name;
    private String lastName;

    public Owner(String name, String lastName) {
        this.name = name;
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }
}
