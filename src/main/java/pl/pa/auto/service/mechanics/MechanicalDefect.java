package pl.pa.auto.service.mechanics;

public enum MechanicalDefect {
    BRAKE_SYSTEM(1500, 7),
    OILS_FILTERS_FLUIDS_BELTS(500, 1),
    COOLING_SYSTEM(800, 3),
    INTAKE_SYSTEM(1000, 7),
    STEERING_SYSTEM(1000, 7),
    DRIVE_SYSTEM(1200, 5),
    FUEL_SYSTEM(500, 3),
    EXHAUST_SYSTEM(450, 5),
    ENGINE(3000, 7),
    SUSPENSION(450, 5);

    private double repairPrice;
    private int repairTimeInDays;

    MechanicalDefect(double repairPrice, int repairTimeInDays) {
        this.repairPrice = repairPrice;
        this.repairTimeInDays = repairTimeInDays;
    }

    public double getRepairPrice() {
        return repairPrice;
    }

    public int getRepairTimeInDays() {
        return repairTimeInDays;
    }
}
