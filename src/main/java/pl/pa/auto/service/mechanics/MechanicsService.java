package pl.pa.auto.service.mechanics;

import pl.pa.auto.service.common.Car;

public class MechanicsService {

    public double calculateRepairPrice(Car car) {
        double repairPrice = 0;

        for (int i = 0; i < car.defectListMechanical().size(); i++) {
            repairPrice += car.defectListMechanical().get(i).getRepairPrice();
        }
        return repairPrice;
    }

    public int calculateRepairTimeInDays(Car car) {
        int repairTimeInDays = 0;

        for (int i = 0; i < car.defectListMechanical().size(); i++) {
            repairTimeInDays += car.defectListMechanical().get(i).getRepairTimeInDays();
        }
        return repairTimeInDays;
    }
}
