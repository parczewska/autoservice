package pl.pa.auto.service.vulcanization;

public enum TireDefect {
    CHANGE_TO_SEASONAL_TIRES(500, 1),
    WHEEL_BALANCING(300, 1),
    TIRE_REPAIR(400, 5),
    TIRE_STORAGE(100, 180),
    VULCANIZATION_TIRES(800, 5);

    private double repairPrice;
    private int repairTimeDays;

    TireDefect(double repairPrice, int repairTimeDays) {
        this.repairPrice = repairPrice;
        this.repairTimeDays = repairTimeDays;
    }

    public double getRepairPrice() {
        return repairPrice;
    }

    public int getRepairTimeDays() {
        return repairTimeDays;
    }
}