package pl.pa.auto.service.vulcanization;

import pl.pa.auto.service.common.Car;

public class VulcanizationService {

    public double calculateRepairPrice(Car car) {
        double repairPrice = 0;

        for (int i = 0; i < car.defectListTire().size(); i++) {
            repairPrice += car.defectListTire().get(i).getRepairPrice();
        }
        return repairPrice;
    }

    public int calculateRepairTimeInDays(Car car) {
        int repairTimeInDays = 0;

        for (int i = 0; i < car.defectListTire().size(); i++) {
            repairTimeInDays += car.defectListTire().get(i).getRepairTimeDays();
        }
        return repairTimeInDays;
    }
}
