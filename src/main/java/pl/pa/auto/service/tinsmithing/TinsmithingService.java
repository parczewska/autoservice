package pl.pa.auto.service.tinsmithing;

import pl.pa.auto.service.common.Car;

public class TinsmithingService {

    public double calculateRepairPrice(Car car) {
        double repairPrice = 0;

        for (int i = 0; i < car.defectListTinsmithing().size(); i++) {
            repairPrice += car.defectListTinsmithing().get(i).getRepairPrice();
        }
        return repairPrice;
    }

    public int calculateRepairTimeInDays(Car car) {
        int repairTimeInDays = 0;

        for (int i = 0; i < car.defectListTinsmithing().size(); i++) {
            repairTimeInDays += car.defectListTinsmithing().get(i).getRepairTimeDays();
        }
        return repairTimeInDays;
    }
}
