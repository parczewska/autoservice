package pl.pa.auto.service.tinsmithing;

public enum TinsmithingDefect {

    REPLACEMENT_OF_FLASHINGS(800, 10),
    REPAIR_OF_FLASHINGS(1200, 10),
    PAINTING_OF_FLASHINGS(3000, 14);

    private double repairPrice;
    private int repairTimeDays;

    TinsmithingDefect(double repairPrice, int repairTimeDays) {
        this.repairPrice = repairPrice;
        this.repairTimeDays = repairTimeDays;
    }

    public double getRepairPrice() {
        return repairPrice;
    }

    public int getRepairTimeDays() {
        return repairTimeDays;
    }
}
