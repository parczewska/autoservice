package pl.pa.auto.service.airconditioning;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.pa.auto.service.common.Car;
import pl.pa.auto.service.common.Owner;
import pl.pa.auto.service.common.Status;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AirConditioningServiceTest {

    private static AirConditioningService airConditioningService = new AirConditioningService();
    private static List<AirConditioningDefect> airConditioningDefectList = new ArrayList<>();
    private static Car car = new Car("", "", new Owner("", ""), Status.DAMAGED, Collections.EMPTY_LIST,
            Collections.EMPTY_LIST, airConditioningDefectList, Collections.EMPTY_LIST, Collections.EMPTY_LIST);

    @BeforeAll
    static void init() {
        airConditioningDefectList.add(AirConditioningDefect.BROKEN);
        airConditioningDefectList.add(AirConditioningDefect.FUNGUS);
        airConditioningDefectList.add(AirConditioningDefect.UNFILLING);
        airConditioningDefectList.add(AirConditioningDefect.LEAKY);
        airConditioningDefectList.remove(AirConditioningDefect.FUNGUS);
    }

    @Test
    void shouldCalculateRepairPriceOfAirConditioning() {
        //given
        double expected = 1000.00;
        //when
        double result = airConditioningService.calculateRepairPrice(car);
        //then
        assertEquals(expected, result);
    }

    @Test
    void shouldCalculateRepairTimeInDaysOfAirConditioning() {
        //given
        int expected = 8;
        //when
        int result = airConditioningService.calculateRepairTimeInDays(car);
        //then
        assertEquals(expected, result);
    }
}
