package pl.pa.auto.service.mechanics;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.pa.auto.service.common.Car;
import pl.pa.auto.service.common.Owner;
import pl.pa.auto.service.common.Status;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MechanicsServiceTest {

    private static MechanicsService mechanicsService = new MechanicsService();
    private static List<MechanicalDefect> mechanicalDefectList = new ArrayList<>();
    private static Car car = new Car("", "", new Owner("", ""), Status.DAMAGED, Collections.EMPTY_LIST,
            mechanicalDefectList, Collections.EMPTY_LIST, Collections.EMPTY_LIST, Collections.EMPTY_LIST);

    @BeforeAll
    static void init() {
        mechanicalDefectList.add(MechanicalDefect.DRIVE_SYSTEM);
        mechanicalDefectList.add(MechanicalDefect.ENGINE);
        mechanicalDefectList.add(MechanicalDefect.BRAKE_SYSTEM);
        mechanicalDefectList.add(MechanicalDefect.FUEL_SYSTEM);
        mechanicalDefectList.add(MechanicalDefect.COOLING_SYSTEM);
        mechanicalDefectList.add(MechanicalDefect.EXHAUST_SYSTEM);
        mechanicalDefectList.add(MechanicalDefect.INTAKE_SYSTEM);
        mechanicalDefectList.add(MechanicalDefect.STEERING_SYSTEM);
        mechanicalDefectList.add(MechanicalDefect.OILS_FILTERS_FLUIDS_BELTS);
        mechanicalDefectList.add(MechanicalDefect.SUSPENSION);
        mechanicalDefectList.remove(MechanicalDefect.BRAKE_SYSTEM);
    }

    @Test
    void shouldCalculateRepairPriceOfMechanics() {
        //given
        double expected = 8900.00;
        //when
        double result = mechanicsService.calculateRepairPrice(car);
        //then
        assertEquals(expected, result);
    }

    @Test
    void shouldCalculateRepairTimeInDaysOfMechanics() {
        //given
        int expected = 43;
        //when
        int result = mechanicsService.calculateRepairTimeInDays(car);
        //then
        assertEquals(expected, result);
    }
}
