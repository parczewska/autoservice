package pl.pa.auto.service.tinsmithing;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.pa.auto.service.common.Car;
import pl.pa.auto.service.common.Owner;
import pl.pa.auto.service.common.Status;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TinsmithingServiceTest {

    private static TinsmithingService tinsmithingService = new TinsmithingService();
    private static List<TinsmithingDefect> tinsmithingDefectList = new ArrayList<>();
    private static Car car = new Car("", "", new Owner("", ""), Status.DAMAGED, Collections.EMPTY_LIST,
            Collections.EMPTY_LIST, Collections.EMPTY_LIST, tinsmithingDefectList, Collections.EMPTY_LIST);

    @BeforeAll
    static void init() {
        tinsmithingDefectList.add(TinsmithingDefect.PAINTING_OF_FLASHINGS);
        tinsmithingDefectList.add(TinsmithingDefect.REPAIR_OF_FLASHINGS);
        tinsmithingDefectList.add(TinsmithingDefect.REPLACEMENT_OF_FLASHINGS);
        tinsmithingDefectList.remove(TinsmithingDefect.PAINTING_OF_FLASHINGS);
    }

    @Test
    void shouldCalculateRepairPriceOfTinsmithing() {
        //given
        double expected = 2000.00;
        //when
        double result = tinsmithingService.calculateRepairPrice(car);
        //then
        assertEquals(expected, result);
    }

    @Test
    void shouldCalculateRepairTimeInDaysOfTinsmithing() {
        //given
        int expected = 20;
        //when
        int result = tinsmithingService.calculateRepairTimeInDays(car);
        //then
        assertEquals(expected, result);
    }
}
