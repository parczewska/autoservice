package pl.pa.auto.service.electronics;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.pa.auto.service.common.Car;
import pl.pa.auto.service.common.Owner;
import pl.pa.auto.service.common.Status;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ElectronicsServiceTest {

    private static ElectronicsService electronicsService = new ElectronicsService();
    private static List<ElectronicsDefect> electronicsDefectList = new ArrayList<>();
    private static Car car = new Car("", "", new Owner("", ""), Status.DAMAGED, Collections.EMPTY_LIST,
            Collections.EMPTY_LIST, Collections.EMPTY_LIST, Collections.EMPTY_LIST, electronicsDefectList);

    @BeforeAll
    static void init() {
        electronicsDefectList.add(ElectronicsDefect.CHANGE_OF_SENSORS);
        electronicsDefectList.add(ElectronicsDefect.SYSTEMS_REPAIR);
        electronicsDefectList.remove(ElectronicsDefect.CHANGE_OF_SENSORS);
    }

    @Test
    void shouldCalculateRepairPriceOfElectronics() {
        //given
        double expected = 300.00;
        //when
        double result = electronicsService.calculateRepairPrice(car);
        //then
        assertEquals(expected, result);
    }

    @Test
    void shouldCalculateRepairTimeInDaysOfElectronics() {
        //given
        int expected = 2;
        //when
        int result = electronicsService.calculateRepairTimeInDays(car);
        //then
        assertEquals(expected, result);
    }
}
