package pl.pa.auto.service.vulcanization;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.pa.auto.service.common.Car;
import pl.pa.auto.service.common.Owner;
import pl.pa.auto.service.common.Status;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class VulcanizationServiceTest {

    private static VulcanizationService vulcanizationService = new VulcanizationService();
    private static List<TireDefect> tireDefectList = new ArrayList<>();
    private static Car car = new Car("", "", new Owner("", ""), Status.DAMAGED, tireDefectList,
            Collections.EMPTY_LIST, Collections.EMPTY_LIST, Collections.EMPTY_LIST, Collections.EMPTY_LIST);

    @BeforeAll
    static void init() {
        tireDefectList.add(TireDefect.VULCANIZATION_TIRES);
        tireDefectList.add(TireDefect.TIRE_STORAGE);
        tireDefectList.add(TireDefect.TIRE_REPAIR);
        tireDefectList.add(TireDefect.CHANGE_TO_SEASONAL_TIRES);
        tireDefectList.add(TireDefect.WHEEL_BALANCING);
        tireDefectList.remove(TireDefect.TIRE_REPAIR);
    }

    @Test
    void shouldCalculateRepairPriceOfVulcanization() {
        //given
        double expected = 1700.00;
        //when
        double result = vulcanizationService.calculateRepairPrice(car);
        //then
        assertEquals(expected, result);
    }

    @Test
    void shouldCalculateRepairTimeInDaysOfVulcanization() {
        //given
        int expected = 187;
        //when
        int result = vulcanizationService.calculateRepairTimeInDays(car);
        //then
        assertEquals(expected, result);
    }
}
